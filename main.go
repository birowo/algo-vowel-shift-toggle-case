package main

import (
	"golang.org/x/exp/constraints"
)

func LoCase(chr rune) rune {
	const (
		aMin1  = 'A' - 1
		zPlus1 = 'Z' + 1
	)
	if chr > aMin1 && chr < zPlus1 {
		return chr - 'A' + 'a'
	}
	return chr
}
func Search[T constraints.Ordered](slc []T, x T) int {
	lo, hi := 0, len(slc)-1
	for lo <= hi {
		mid := int(uint(lo+hi) >> 1) // avoid overflow
		y := slc[mid]
		if x == y {
			return mid
		}
		if x < y {
			hi = mid - 1
		} else {
			lo = mid + 1
		}
	}
	return -1
}
func vowelShiftToggleCase(str string) string {
	runes := []rune(str)
	loVowelK := []rune("aeiou")
	upVowelV := []rune("EIOUA")
	loVowelV := []rune("eioua")
	for i, chr := range runes {
		loCase := LoCase(chr)
		if idx := Search(loVowelK, loCase); idx > -1 {
			if chr == loCase { //if lower-case ...
				runes[i] = upVowelV[idx]
			} else {
				runes[i] = loVowelV[idx]
			}
		}
	}
	return string(runes)
}
func main() {
	print(vowelShiftToggleCase("qwertyuiop世界QWERTYUIOP世界"))
}

